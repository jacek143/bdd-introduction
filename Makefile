BDD.pdf: BDD.tex dry_meme.pdf use_case.pdf bdd_mind_map.pdf
	pdflatex BDD.tex

dry_meme.pdf: dry_meme.tex
	pdflatex dry_meme.tex

use_case.pdf: use_case.tex
	pdflatex use_case.tex

bdd_mind_map.pdf: bdd_mind_map.tex
	pdflatex bdd_mind_map.tex
